<?php

/**
 * Notes:HTTP服务端
 * @Author szh
 * Class HTTP
 * 启动
 * php HTTP.php
 * 访问
 * 浏览器访问 地址:端口  http://localhost:9501?hello=666
 */
class HTTP
{
    private $http = null;

    public function __construct()
    {
        $this->http = new Swoole\Http\Server('0.0.0.0', 9501);
//        $this->http->set([
//            'enable_static_handle'   => true,     //
//            'document_root'    => "",     //
//        ]);
        $this->http->on('Request', [$this, "onRequest"]);
        $this->http->start();
    }
    public function onRequest($request, $response){
        //Chrome 浏览器访问服务器，会产生额外的一次请求，/favicon.ico，可以在代码中响应 404 错误。
        if ($request->server['path_info'] == '/favicon.ico' || $request->server['request_uri'] == '/favicon.ico') {
            $response->end();
            return;
        }
        var_dump($request->get,$request->post);
        $response->header('Content-Type', 'text/html; charset=utf-8');
        $response->end('<h1>Hello Swoole. #' . json_encode($request->get) . '</h1>');
    }
}
new HTTP();