<?php

/**
 * Notes:TCP服务端
 * @Author szh
 * Class TCP
 * 启动
 * php TCP.php
 */
class TCP
{
    private $server = null;

    public function __construct()
    {
        $this->server = new Swoole\Server('127.0.0.1', 9503);
        $this->server->set([
            //'reactor_num'   => 2,     // reactor thread num
            'worker_num'    => 4,     // worker process num 一般是系统核数2~4倍
            //'backlog'       => 128,   // listen backlog
            'max_request'   => 50,
            //'dispatch_mode' => 1,
        ]);
        $this->server->on('start', [$this, "onStart"]);
        $this->server->on('connect', [$this, "onConnect"]);
        $this->server->on('receive', [$this, "onReceive"]);
        $this->server->start();
    }
    public function onConnect($server, $fd){
        echo "客户端id：{$fd} 连接.".PHP_EOL;
    }
    public function onReceive($server, $fd, $reactor_id, $data){
        $server->send($fd, "发送的数据：".$data);
    }
    public function onClose($server, $fd){
        echo "客户端id：{$fd} 断开.".PHP_EOL;
    }
}
new TCP();