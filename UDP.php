<?php

/**
 * Notes:UDP服务端
 * @Author szh
 * Class UDP
 * 启动
 * php UDP.php
 */
class UDP
{
    private $server = null;

    public function __construct()
    {
        $this->server = new Swoole\Server('127.0.0.1', 9502, SWOOLE_PROCESS, SWOOLE_SOCK_UDP);
        $this->server->set([
            //'reactor_num'   => 2,     // reactor thread num
            'worker_num'    => 4,     // worker process num 一般是系统核数2~4倍
            //'backlog'       => 128,   // listen backlog
            'max_request'   => 50,
            //'dispatch_mode' => 1,
        ]);
        $this->server->on('Packet', [$this, "onPacket"]);
        $this->server->start();
    }
    public function onPacket($server, $data, $clientInfo){
        var_dump($clientInfo);
        $server->sendto($clientInfo['address'], $clientInfo['port'], "Server：{$data}");
    }
}
new UDP();