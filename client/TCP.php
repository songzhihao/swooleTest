<?php
use Swoole\Coroutine\Client;
use function Swoole\Coroutine\go;
/**
 * Notes:TCP客户端
 * @Author szh
 * Class TCP
 * 启动
 * php TCP.php
 */
go(function () {
    $client = new Client(SWOOLE_SOCK_TCP);
    if (!$client->connect('127.0.0.1', 9501, 0.5))
    {
        echo "connect failed. Error: {$client->errCode}\n";
    }
    /*// 测试开启终端输入
    fwrite(STDOUT, "请输入：");
    $res = fgetc(STDIN);
    $client->send("hello world $res \n");*/
    $client->send("hello world \n");
    echo $client->recv();
    $client->close();
});
